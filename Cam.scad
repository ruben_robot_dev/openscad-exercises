r1 = 25;
r2 = 10;
th = 5;
d = 40;
hull()
{
  translate([-d/2,0,0])
    cylinder(r=r1);

  translate([d/2,0,0])
    cylinder(r=r2);
}
