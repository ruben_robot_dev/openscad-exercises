module rcylinder(center=false, both=false, r=1, h=1, $fn=10)
{
  //-- center option
  A=center ? [0,0,-h/2] : [0,0,0];
  translate(A)
  {
    union()
    {
      //-- both sides option
      if(both)
      {
        translate([0,0,h/2])
          cylinder(r=r,h=h-2*r, center=true);
        translate([0,0,h-r])
          sphere(r=r,$fn=$fn);
        translate([0,0,r])
          sphere(r=r,$fn=$fn);
      }
      //-- one side option
      else
      {
        translate([0,0,(h-r)/2])
          cylinder(r=r,h=h-r, center=true);
        translate([0,0,h-r])
          sphere(r=r,$fn=$fn);
      }
    }
  }
}

//-- separation variables
d=5;

//-- Simple cylinder
color("yellow")
  translate([0,-d*2,0])
    cylinder(r=1,h=10,center=true,$fn=10);
//-- cylinder with only one side rounded
color("red")
  translate([0,-d,0])
    rcylinder(h=10, center=true);
//-- cylinder with only both sides rounded
color("blue")
  translate([0,0,0])
    rcylinder(h=10, both=true, center=true);
//-- cylinder with only one side rounded and bigger radius
color("magenta")
  translate([0,d,0])
    rcylinder(h=10,r=2);
//-- cylinder with only both sides rounded and bigger radius
color("purple")
  translate([0,2*d,0])
    rcylinder(h=10,r=2,both=true);
