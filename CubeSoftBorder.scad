l1=10;
l2=10;
r=3;
h=10;
//-- Simple cube example
translate([-20,0,0])
  cube([10,10,10], center=true);

//-- union cylinders to cube
p1=l1/2-r;
p2=l2/2-r;

hull()
{
  translate([p1,p2,-h/2])
    cylinder(r=r, h=h);
  translate([-p1,p2,-h/2])
    cylinder(r=r, h=h);
  translate([-p1,-p2,-h/2])
    cylinder(r=r, h=h);
  translate([p1,-p2,-h/2])
    cylinder(r=r, h=h);
}

//--simplify the cylinders cube
POINTS = [
          [p1,p2,-h/2],
          [p1,-p2,-h/2],
          [-p1,-p2,-h/2],
          [-p1,p2,-h/2],
        ];

translate([20,0,0])
{
  %hull()
  {
    for(pos=POINTS)
    {
      translate(pos)
        #cylinder(r=r, h=h);
    }
  }
}
