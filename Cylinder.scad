//-- Cylinder
cylinder(r=20,h=20);

//-- Other cylinder softer
translate([50,0,0])
  cylinder(r=20,h=20, $fn=100);

//-- Change cylinder to other geometric

//-- Pentagon
translate([-50,0,0])
  cylinder(r=20,h=20, $fn=5);
  
//-- hexagon
translate([-100,0,0])
  cylinder(r=20,h=20, $fn=6);
  
//-- equilateral triangle
translate([-150,0,0])
  cylinder(r=20,h=20, $fn=3);
