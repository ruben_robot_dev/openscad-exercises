use <WheelCopy2.scad>

wheelRadius=30;
n=8;

module drill(radius=1)
{
    cylinder(r=radius, 
            h=100,
            $fn=100,
            center=true);
}
difference(){
    wheel(wheelRadius=wheelRadius);
    for (i=[0:n-1])
    {
        rotate([0,0,i*45])
            translate([20,0,0])
                drill(radius=3);
    }
}