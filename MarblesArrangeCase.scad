use <ModuleSphereArray.scad>
w=3;d=3;
//-- Marbles saver
difference()
{
  //-- Base
  translate([0,0,-2.5])
    cube([w*10,d*10,5], center=true);
  //-- Marbles array
  array(w=w,d=d,r=4);
}
