//-- Orthogonal system
X=0;
Y=1;
Z=2;

//-- Cubes sizes
A=[30,30,10];
B=[10,10,30];
C=[8,8,8];
D=[5,5,8];

//-- Position Cubes
posZB=A[Z]/2+B[Z]/2;
posZC=posZB+B[Z]/2+C[Z]/2;
posZD=posZC+C[Z]/2+D[Z]/2;

POSITION = [
              [0,0,posZB],
              [0,0,posZC],
              [0,0,posZD],
            ];

//-- Draw the Cubes
cube(A,center=true);
translate(POSITION[0])
  cube(B,center=true);
translate(POSITION[1])
  cube(C,center=true);
translate(POSITION[2])
  cube(D,center=true);
