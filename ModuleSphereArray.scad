module array(w=2, h=2, d=2,r=2, size=[10,10,10],q=5)
{
  translate([-(w-1)*size[0]/2,-(d-1)*size[1]/2,-(h-1)*size[2]/2])
  for(k=[0:h-1])
  {
    for(j=[0:d-1])
    {
      for(i=[0:w-1])
      {
        translate([i*size[0], j*size[1], k*size[2]])
          sphere(r=r,$fn=q);
      }
    }
  }
}

array(w=3,d=3,r=5);
