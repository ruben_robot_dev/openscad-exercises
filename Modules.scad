use <WheelCopy2.scad>
//First pair wheels
rotate([90,0,0]){
    //--Back Wheel
    translate([0,0,20])
        {
            wheel(wheelRadius=20,
            axleRadius=5);
        }
    //-- Front Wheel
    translate([60,0,20])
        {
            wheel(wheelRadius=30, 
            axleRadius=5,wheelHeigth=8);
        }
}
//Second pair wheels
mirror([0,1,0])
{
    rotate([90,0,0]){
        //--Back Wheel
        translate([0,0,20])
            {
                wheel(wheelRadius=20,
                axleRadius=5);
            }
        //-- Front Wheel
        translate([60,0,20])
            {
                wheel(wheelRadius=30, 
                axleRadius=5,wheelHeigth=8);
            }
    }
}
//-- Chasis
translate([40,0,0])
cube([100,39,10], center=true);