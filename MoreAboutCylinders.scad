d=50;

//-- Normal cylinder
translate([0,0,0])
  cylinder(r=20,h=20,$fn=100);

//Draw a truncated cone with a cylinder
translate([d,0,0])
  cylinder(r1=20,r2=10,h=20,$fn=100);

//Draw a cone with a cylinder
translate([2*d,0,0])
  cylinder(r1=20,r2=0,h=20,$fn=100);

//Draw a pyramid maya with a cylinder
translate([0,d,0])
  cylinder(r1=20,r2=10,h=20,$fn=4);

//Draw a pyramid with a cylinder
translate([d,d,0])
  cylinder(r1=20,r2=0,h=20,$fn=4);

//Draw a prism
translate([2*d,d,0])
  cylinder(r=20,h=20,$fn=3);
