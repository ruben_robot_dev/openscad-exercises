//-- Orthogonal system
X=0;
Y=1;
Z=2;

//-- Cubes size
A=[30,30,10];
B=[10,10,30];

//-- Positions of cubes

POSITION =  [
              0,0,A[Z]/2+B[Z]/2
            ];

//-- Draw cubes

cube(A,center=true);
translate(POSITION)
  color("red")
    cube(B,center=true);
