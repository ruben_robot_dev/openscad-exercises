//-- New wheel 
difference()
{
    //-- Wheel structure
    union()
    {
        //-- Whell
        cylinder(r=50, h=5, $fn=100);
        
        //-- connector
        cylinder(r=20, h=20, $fn=100);
    }
    //-- Axle hole
    cylinder(r=8,h=50,center=true);
}