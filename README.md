# OpenSCAN exercises
## Exercise 1

Image base:

![ex1](./static/ex-01-01.png "This the exercise 1")

Solution:

![ex1](./static/ex-01-01-scad.png "This the exercise 1")

**In the reference is skipped the diameter of groove I take this as 28**

## Exercise 2

Image base:

![ex2](./static/ex-01-02.png "This the exercise 2")

Solution:

![ex2](./static/ex-01-02-scad.png "This the exercise 2")

## Exercise 3

Image base:

![ex3](./static/ex-01-03.png "This the exercise 3")

Solution:

![ex3](./static/ex-01-03-scad.png "This the exercise 3")

## Exercise 4

Image base:

![ex4](./static/ex-01-04.png "This the exercise 4")

Solution:

![ex4](./static/ex-01-04-scad.png "This the exercise 4")

## Exercise 5

Image base:

![ex5](./static/ex-01-05.png "This the exercise 5")

Solution:

![ex5](./static/ex-01-05-scad.png "This the exercise 5")

## Exercise 6

Image base:

![ex6](./static/ex-01-06.png "This the exercise 6")

Solution:

![ex6](./static/ex-01-06-scad.png "This the exercise 6")

## Exercise 7

Image base:

![ex7](./static/ex-01-07.png "This the exercise 7")

Solution:
<!-- 
![ex7](./static/ex-01-07-scad.png "This the exercise 7") -->
