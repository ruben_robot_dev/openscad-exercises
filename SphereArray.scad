distance = 50;
radius = 5;
width = 5;
heigth = 10;
deep = 2;
translate([-distance*(width-1)/2,
          -distance*(deep-1)/2,
          -distance*(heigth-1)/2])
{
  for(k=[0:heigth-1])
  {
    for(j=[0:deep-1])
    {
      for(i=[0:width-1])
      {
        translate([i*distance,j*distance,k*distance])
          sphere(r=radius);
      }
    }
  }
}
