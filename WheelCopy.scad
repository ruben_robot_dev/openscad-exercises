wheelRadius = 50;
axleRadius = 20;
wheelHeigth = 5;

difference()
{
    //-- Wheel 
    cylinder(r=wheelRadius,
    h=wheelHeigth,
    $fn=100);
    //-- Axle hole
    cylinder(r=axleRadius, 
    h=wheelHeigth+10,
    $fn=100, 
    center=true);
}
