module wheel(wheelRadius=10, 
                axleRadius=8,
                wheelHeigth=5)
{
    difference()
    {
        cylinder(r = wheelRadius,
        h = wheelHeigth,
        $fn = 100);
        cylinder(r = axleRadius,
        h = wheelHeigth+10,
        $fn = 100, center=true);
    }
}

wheel(wheelRadius=12);

translate([-40,0,0])
  wheel();
translate([40,0,0])
  wheel(wheelRadius=20,
        wheelHeigth=5,
        axleRadius=3);