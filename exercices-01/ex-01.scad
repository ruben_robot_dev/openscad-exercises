/* Rubén Darío Jordan Sáenz */
/* ansys 3D Exercises by Sachindanand Jha editorial cdin 360°*/
/* first exercise */

/* --- Variables --- */
ExternalDiameter = 80;
GrooveDiameter = 28;
CenterHoleDiameter = 28;
Holes = 10;
PieceHeigth = 5;
HolesNumber=3;

/* Piece */
difference()
{
  /* Cylinder Base */
  cylinder(d=ExternalDiameter,h=PieceHeigth, $fn=100,center=true);
  /* Holes and Groove */
  union()
  {
    /* Groove */
    hull()
    {
      /* First groove cylinder */
      cylinder(d=GrooveDiameter,h=PieceHeigth+1, $fn=100,center=true);
      /* Second groove cylinder */
      translate([ExternalDiameter/2,0,0])
        cylinder(d=GrooveDiameter,h=PieceHeigth+1, $fn=100,center=true);
    }
    /* Holes */
    for(i=[0:HolesNumber])
    {
      rotate([0,0,i*90])
        translate([0,CenterHoleDiameter,0])
          cylinder(d=Holes,h=PieceHeigth+1, $fn=100,center=true);
    }
  }
}
