/* Rubén Darío Jordan Sáenz */
/* ansys 3D Exercises by Sachindanand Jha editorial cdin 360°*/
/* first exercise */

/* Variables */
/* Base variables */
LengthBase = 120;
WidthBase  = 100;
HeigthBase = 20;
RadiusShapeBase = 30;
GrooveRadius = 15;
GrooveCenterDistance = 60;

/* Secons section variables */
WidthSecond = 20;
HeigthSecond = 30;
RadiusShapeSecond = 20;
GrooveRadiusSecond = 6;
GrooveCenterDistanceSecond = 80;

/* Piece */
/* Groove module to simplify the code */
module Groove(D=10, r=10, h=5, $fn=100)
{
  /* Union of perforarion */
  hull()
  {
    /* First cylinder */
    translate([0,D/2,0])
      cylinder(r=r,h=h,$fn=$fn,center=true);
    /* Second cylinder */
    translate([0,-D/2,0])
      cylinder(r=r,h=h,$fn=$fn,center=true);
  }
}

/* --- Piece ---- */
union()
{
  /* Base */
  translate([0,0,-HeigthBase/2])
    /* Base construction */
    difference()
    {
      translate([WidthBase/2,0,0])
      /* Union of corners */
      hull()
      {
        /* Back square base corners */
        translate([-WidthBase/2+RadiusShapeBase,0,0])
          cube([RadiusShapeBase*2,LengthBase,HeigthBase],center=true);
        /* Front Rounded Base corners */
        translate([WidthBase/2-RadiusShapeBase,0,0])
          Groove(D=GrooveCenterDistance,r=RadiusShapeBase,h=HeigthBase);
      }
      /* Groove Perforation */
      translate([WidthBase-RadiusShapeBase,0,0])
        Groove(D=GrooveCenterDistance,r=GrooveRadius,h=HeigthBase+1);
    }

  /* --- Second --- */
  translate([WidthSecond/2,0,0])
    rotate([0,90,0])
      difference()
      {
        hull()
        {
          /* Top Rounded second corners */
          translate([-(HeigthSecond)/2,0,0])
            Groove(r=RadiusShapeSecond, D=GrooveCenterDistanceSecond, h=WidthSecond);
          /* Botton square second corners */
          cube([RadiusShapeBase/2,LengthBase,WidthSecond], center=true);
        }
        /* Groove Perforation */
        translate([-(HeigthBase+WidthSecond)/2,0,0])
          Groove(r=GrooveRadiusSecond, D=GrooveCenterDistanceSecond, h=WidthSecond+1);
      }
}
