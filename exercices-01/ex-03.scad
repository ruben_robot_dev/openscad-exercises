/* Rubén Darío Jordan Sáenz */
/* ansys 3D Exercises by Sachindanand Jha editorial cdin 360°*/

/* Variables */
/* Base variables */
LenghtBase = 60;
WidthBase = 100;
HeigthBase = 20;

/* second variables */
LenghtSecond=20;
WidthSecond=100;
HeigthSecond=50;
ExternalRadius = 25;
InternalDiameter = 40;

/* Piece */
union()
{
  /* Base */
  translate([0,-WidthBase/2,0])
    cube([LenghtBase,WidthBase,HeigthBase]);

  /* Second */
  difference()
  {
    union()
    {
      translate([0,-WidthBase/2,HeigthBase])
        cube([LenghtSecond,WidthSecond,HeigthSecond/2]);
      translate([0,0,HeigthBase+HeigthSecond/2])
        rotate([0,90,0])
          cylinder(r=ExternalRadius,h=LenghtSecond);
    }
    translate([0,0,HeigthBase+HeigthSecond/2])
      rotate([0,90,0])
        translate([0,0,-.5])
          cylinder(d=InternalDiameter,h=LenghtSecond+1);
  }
}
