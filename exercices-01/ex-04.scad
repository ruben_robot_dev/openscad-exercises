/* Rubén Darío Jordan Sáenz */
/* ansys 3D Exercises by Sachindanand Jha editorial cdin 360°*/

/* Variables */
/* Base variables */
DiameterBase = 20;
HeigthBase = 30;
/* Second Variables */
DiameterSecond = 40;
HeigthSecond = 30;

/* Piece */
union()
{
  cylinder(d=DiameterBase, h=HeigthBase);
  translate([0,0,HeigthBase])
    cylinder(d=DiameterSecond, h=HeigthSecond);
}
