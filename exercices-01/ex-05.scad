/* Rubén Darío Jordan Sáenz */
/* ansys 3D Exercises by Sachindanand Jha editorial cdin 360°*/

/* Variables */
LengthBase = 40;
Border2CenterBase = 130;
HeightBase = 10;
GrooveRadius = 10;
LengthGroove = 40;
/* Second */
WidthSecond = 50;
CenterToBase = 40;
InternalDiameter = 30;
TabsLength = 10;

/* Piece */
module Groove(r=0,h=0,d=0)
{
  hull()
  {
    cylinder(r=r, h=h);
    translate([d,0,0])
      cylinder(r=r, h=h);
  }
}
union()
{
  difference()
  {
    union()
    {
      cylinder(d=LengthBase,h=HeightBase);
      translate([0,-LengthBase/2,0])
        cube([Border2CenterBase,LengthBase,HeightBase]);
    }
    translate([0,0,-0.5])
      Groove(r=GrooveRadius, h=HeightBase+1,d=LengthGroove);
  }
  translate([Border2CenterBase-WidthSecond/2,0,0])
  {
    difference()
    {
      union()
      {
        translate([0,0,CenterToBase])
          rotate([90,0,0])
            cylinder(d=WidthSecond,h=LengthBase,center=true);
        translate([0,0,CenterToBase/2])
          cube([WidthSecond,LengthBase,CenterToBase],center=true);
      }
      union()
      {
        translate([-(WidthSecond+1)/2,-(LengthBase-2*TabsLength)/2,HeightBase])
          cube([WidthSecond+1,LengthBase-2*TabsLength,CenterToBase+InternalDiameter]);
        translate([0,0,CenterToBase])
          rotate([90,0,0])
            cylinder(d=InternalDiameter,h=LengthBase+1,center=true);
      }
    }
  }
}
