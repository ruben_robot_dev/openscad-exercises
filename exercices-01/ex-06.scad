/* Rubén Darío Jordan Sáenz */
/* ansys 3D Exercises by Sachindanand Jha editorial cdin 360°*/

/* Variables */
BaseWidth = 10;
BaseLength = 70;
BaseHeight = 30;
TopCubeAxis = 10;
Points = [
  [-0.5,0,0],[11,0,0]
];

/* Piece */
module trianglePrism(l=10,h=10,w=10)
{
  difference()
  {
    cube([l,w,h]);
    angle = atan(h/l);
    rotate([0,-angle,0])
      translate([0,-0.5,0])
        cube([2*l,w+1,2*h]);
  }
}
difference()
{
  cube([BaseLength,BaseWidth,BaseHeight]);
  union()
  {
    translate([(BaseLength-TopCubeAxis)/2,-0.5,BaseHeight-TopCubeAxis+0.003])
      cube([TopCubeAxis+1,TopCubeAxis+1,TopCubeAxis]);
    translate([BaseLength/2,0,0])
      union()
      {
        translate([10,-0.5,-0.003])
          trianglePrism(l=15,w=11,h=10);
        mirror()
          translate([10,-0.5,-0.003])
            trianglePrism(l=15,w=11,h=10);
      }
  }
}
