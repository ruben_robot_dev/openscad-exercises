R=10;
n=3;

module drill(radius=5)
{
    cylinder(r=radius, 
    h=R*2+5, 
    center=true, 
    $fn=100);
}
difference()
{
    sphere(r=R,center=true,$fn=100);
    union()
    {
        for(i=[0:n-2])
        {   
            rotate([0,i*90,0])
                drill();
        }
        rotate([90,0,0])
            drill();
        #rotate([90,0,0])
            drill();
    }
}