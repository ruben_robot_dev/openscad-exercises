union()
{
  color("Blue")
    sphere(r=10, $fn=100);
  color("Red")
    cylinder(r=5, h=20, center=true);
}
