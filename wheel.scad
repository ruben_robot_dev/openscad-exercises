//--wheel
difference()
{
//-- wheel cylinder
cylinder(r=50,h=5, $fn=100);

//-- axle hole
cylinder(r=8,h=20,$fn=100,center=true);
}
